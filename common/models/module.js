var Q = require( 'q' ),
  _ = require( 'lodash' );

module.exports = function ( Module ) {

  setInterval( function () {
    getLatest().then( function ( latest ) {
      Module.app.io.emit( 'server:message', latest );
      console.log( 'server:message', latest );
    }, function ( err ) {
      Module.app.io.emit( 'server:err', err );
      console.log( 'server:err', err );
    } );
  }, 3000 );

  function getLatest() {
    return Q.Promise( function ( resolve, reject ) {
      getAllModules().then( function ( modules ) {
        var latest = {};
        var latestReadingsPromises = [];
        _.each( modules, function ( module ) {
          latest[ module.deviceUid ] = [];
          var moduleLatest = latest[ module.deviceUid ];
          return module.sensors( function ( err, sensors ) {
            _.each( sensors, function ( sensor ) {
              var readingsPromise = getLatestReadings( sensor ).then( function ( readings ) {
                latest[ module.deviceUid ].push( { type: sensor.type, readings: readings } );
              } );
              latestReadingsPromises.push( readingsPromise );
            } )
          } );
        } );
        Q.all( latestReadingsPromises ).then( function () {
          resolve( latest );
        } )
      } );
    } );
  }

  function getAllModules() {
    return Module.find( {
      include: { relation: 'sensors' }
    } );
  }


  /**@returns promise latest readings
   */
  function getLatestReadings( sensor, limit ) {
    limit = limit || 1;
    return sensor.readings( { order: 'date DESC', limit: limit } );
  }


};
