var loopback = require( 'loopback' );
var boot = require( 'loopback-boot' );
var bodyParser = require( 'body-parser' );

var app = module.exports = loopback();
//app.use(bodyParser.json());
app.use( bodyParser.urlencoded( { extended: true } ) );
app.start = function () {
  // start the web server
  return app.listen( function () {
    app.emit( 'started' );
    console.log( 'Web server listening at: %s', app.get( 'url' ) );
  } );
};


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot( app, __dirname, function ( err ) {
  if ( err ) throw err;

  // start the server if `$ node server.js`
  if ( require.main === module ) {

    app.io = require( 'socket.io' )( app.start() );
    //require( 'socketio-auth' )( app.io, {
    //  authenticate: function ( value, callback ) {
    //
    //    var AccessToken = app.models.AccessToken;
    //    //get credentials sent by the client
    //    var token = AccessToken.find( {
    //      where: {
    //        and: [ { userId: value.userId }, { id: value.id } ]
    //      }
    //    }, function ( err, tokenDetail ) {
    //      if ( err ) throw err;
    //      if ( tokenDetail.length ) {
    //        callback( null, true );
    //      } else {
    //        callback( null, false );
    //      }
    //    } ); //find function..
    //  } //authenticate function..
    //} );

    app.io.on( 'connection', function ( socket ) {
        console.log( 'a user connected' );
        app.io.emit( 'server:connected', 'connected!' );
        socket.on( 'disconnect', function () {
          console.log( 'user disconnected' );
        } );
        socket.on( 'client:message', function ( message ) {
          console.log( 'a user message ', message );
        } );
      }
    );

  }
} )
;
