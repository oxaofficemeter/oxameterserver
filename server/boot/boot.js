/**
 * Created by yan on 31.08.15.
 */
module.exports = function ( app ) {
  // Install a "/ping" route that returns "pong"
  var _ = require( 'lodash' );
  var Q = require( 'q' );

  function getModule( id ) {
    return Q.Promise( function ( resolve, reject, notify ) {
      app.models.Module.findById( id, function ( err, module ) {
        if ( err ) {
          reject( err );
        } else if ( module === null ) {
          app.models.Module.create( { deviceUid: id }, function ( err, module ) {
            if ( err ) {
              reject( err );
            }
            resolve( module );
          } )
        } else {
          resolve( module );
        }
      } );
    } );
  }

  function getSensor( module, sensorType ) {
    return Q.Promise( function ( resolve, reject, notify ) {
      module.sensors( { type: sensorType }, function ( err, sensors ) {
        if ( err ) {
          reject( err );
        } else if ( !sensors[ 0 ] ) {
          module.sensors.create( { type: sensorType }, function ( err, sensor ) {
            if ( err ) {
              reject( err );
            }
            resolve( sensor );
          } )
        } else {
          resolve( sensors[ 0 ] );
        }
      } );
    } );
  }

  app.post( '/sensor', function ( req, res ) {
    var body = req.body;
    /** @namespace body.temperature */
    /** @namespace body.deviceUid */
    if ( body.deviceUid ) {
      app.io.emit( 'server:new_reading', body );
      console.log( body.deviceUid );
      getModule( body.deviceUid ).then( function ( module ) {
          _.each( [ 'temperature', 'humidity', 'co2' ], function ( sensorType ) {
            if ( body[ sensorType ] ) {
              var value = body[ sensorType ];
              getSensor( module, sensorType ).then( function ( sensor ) {
                sensor.readings.create( { value: value }, function ( reading ) {
                } );
              } );
            }
          } );
          res.send( 'OK' );
        }
      )
      ;
    }
  } );
};
