/* global toastr:false, moment:false */
(function() {
  'use strict';

  angular
    .module('oxameterserver')
    .constant('socket.io-client', io)
    .constant('toastr', toastr)
    .constant('moment', moment);

})();
