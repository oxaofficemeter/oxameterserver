(function() {
  'use strict';

  angular
    .module('oxameterserver')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
