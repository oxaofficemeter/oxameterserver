(function () {
  'use strict';

  angular
    .module( 'oxameterserver', [ 'ngResource', 'ui.router', 'ui.bootstrap', 'btford.socket-io' ] );

})();
