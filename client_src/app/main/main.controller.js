(function() {
  'use strict';

  angular
    .module('oxameterserver')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, clientSocket) {
    var vm = this;

    clientSocket.on( 'server:new_reading', function ( body ) {
      toastr.info(body);
    });

  }
})();
