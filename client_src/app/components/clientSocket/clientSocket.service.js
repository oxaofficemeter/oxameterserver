(function () {
  'use strict';

  angular
    .module( 'oxameterserver' )
    .factory( 'clientSocket', clientSocket );

  /** @ngInject */
  function clientSocket( socketFactory ) {
    return socketFactory();
  }
})();
