(function () {
  'use strict';

  angular
    .module( 'oxameterserver' )
    .directive( 'acmeCurrentReadings', acmeCurrentReadings );

  /** @ngInject */
  function acmeCurrentReadings() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/currentReadings/currentReadings.html',
      controller: CurrentReadingsController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function CurrentReadingsController( clientSocket ) {
      var vm = this;

      console.log( 'CurrentReadingsController' );
      clientSocket.on( 'server:message', function ( data ) {
        vm.readings = data;
      } );
    }
  }

})();
